package golang_logger

import (
	"testing"
)

func TestLog(t *testing.T) {
	logger, err := CreateLogger("test")
	if err != nil {
		t.Error("Error: ", err)
	}
	logger.Log("test line");
}