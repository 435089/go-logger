package golang_logger

import (
	"fmt"
	"strings"
	"log"
	"time"
	"os"
)

type Logger struct {
	LogFilePath	string
	LogFile		*os.File
	LogFileName string
}

func CreateLogger(filePathInput string) (*Logger, error) {
	filePath := strings.Trim(filePathInput, " ")

	if len(filePath) == 0 {
		return nil, fmt.Errorf("The log file path is required")
	}

	return &Logger{
		LogFilePath: filePath,
	}, nil
}

func (this *Logger) Log(message string) {
	logFileName := time.Now().Format("20060102") + ".log"

	_, err := os.ReadDir(this.LogFilePath)
	if err != nil {
		if os.IsNotExist(err) {
			err = os.Mkdir(this.LogFilePath, 0750)
			if err != nil {
				log.Fatal(err)
			}
		}
	}

	if logFileName != this.LogFileName {
		this.LogFile.Close()
		newFile, err := os.OpenFile(this.LogFilePath + "/" + logFileName, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
		if err != nil {
			log.Fatal(err)
		}

		this.LogFileName = logFileName
		this.LogFile = newFile

		log.SetOutput(this.LogFile)
	}
	
	log.Println(message)
}